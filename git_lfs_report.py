#!/usr/bin/python3
'''
Git LFS reporting command

Writes a json formatted structure to stdout
'''

import json
import logging
import requests
from subprocess import run, PIPE

GIT_REMOTE_COMMAND = "git remote get-url origin"
GIT_LFS_LS_COMMAND = "git lfs ls-files --long --all --deleted"
GIT_SHA_COMMAND = "git rev-parse HEAD"
GIT_BRANCH_COMMAND = "git rev-parse --abbrev-ref HEAD"


def sys_command(command):
    '''Run a system command, and return a nicely formatted string'''
    return run(command.split(), stdout=PIPE, check=True).stdout \
        .decode().strip()


def get_sha():
    '''Get the current sha'''
    return sys_command(GIT_SHA_COMMAND)


def get_branch():
    '''Get the current branch'''
    return sys_command(GIT_BRANCH_COMMAND)


def get_remote():
    '''Get the git remote'''
    return sys_command(GIT_REMOTE_COMMAND)


def get_lfs_refs():
    '''
    Get dict of lfs remotes

    Return format is:

    {
        "remote": "<GIT REMOTE NAME>",
        "refs": {
            "<sha oid ref>": "<* or -, per git-lfs ls-files doc>"
        }
    }
    '''
    lfs_refs = [x for x
                in sys_command(GIT_LFS_LS_COMMAND).split("\n")
                if x != '']

    if lfs_refs == []:
        return dict()

    def format_lfs_ref(item):
        '''
        Formatter for the individual lfs_refs

        Returns a tuple comprising the oid and the path.
        '''
        (oid, _, path) = item.split(maxsplit=2)
        return (oid, path)

    return dict(format_lfs_ref(ref) for ref in lfs_refs)


def create_lfs_report():
    '''Create a report structure'''
    return {
        "remote": get_remote(),
        "branch": get_branch(),
        "sha": get_sha(),
        "refs": get_lfs_refs(),
    }


def send_json_report(data):
    '''Send the report to the bannerboy firebase instance'''
    url = 'https://us-central1-bb-banner-admin.cloudfunctions.net' + \
        '/addLfsListing'
    return requests.post(url, json=data)


def configure_logging(level=logging.INFO):
    '''Set up logging for the routine'''
    logging.basicConfig(level=level)
    this_logger = logging.getLogger('git_lfs_report')
    this_logger.setLevel(level)
    return this_logger


if __name__ == "__main__":
    log = configure_logging()
    report = create_lfs_report()
    log.info(json.dumps(report, indent=4, sort_keys=True))
    log.info('Sending data to firebase')
    try:
        send_json_report(report)
    except Exception as error:
        log.error(error)
