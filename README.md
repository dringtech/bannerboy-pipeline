# Bannerboy pipeline environment

This is a docker image which supports the Bitbucket build process for
Bannerboy. It collects information about the LFS files in a git repo for
external storage. This is designed to help in the event of corruption of the
Git LFS configuration of a repo.

## Building and testing locally

1. From this directory:

    docker build --tag dringtech/bannerboy-pipeline:local .

2. From a `git lfs` enabled repository

    docker run -it --volume=$(pwd):/localDebugRepo --workdir="/localDebugRepo" --memory=4g --memory-swap=4g --memory-swappiness=0 dringtech/bannerboy-pipeline:local git_lfs_report.py
